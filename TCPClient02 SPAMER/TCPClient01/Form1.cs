﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace TCPClient01
{
    public partial class Form1 : Form
    {
        TcpClient mTcpClient;
        TcpClient mTcpClient1;
        TcpClient mTcpClient2;
        TcpClient mTcpClient3;
        TcpClient mTcpClient4;
        TcpClient mTcpClient5;
        TcpClient mTcpClient6;
        TcpClient mTcpClient7;
        TcpClient mTcpClient8;
        TcpClient mTcpClient9;
        byte[] mRx;
        string lastRecv = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            tbConsole.Clear();
            IPAddress ipa;
            int nPort;

                if (string.IsNullOrEmpty(tbServerIP.Text) || string.IsNullOrEmpty(tbServerPort.Text))
                    return;
                if (!IPAddress.TryParse(tbServerIP.Text, out ipa))
                {
                    MessageBox.Show("Please supply an IP Address.");
                    return;
                }

                if (!int.TryParse(tbServerPort.Text, out nPort))
                {
                    nPort = 23000;
                }
            while (1 == 1)
            {
                try {
                    mTcpClient = new TcpClient();
                    mTcpClient.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient);

                    mTcpClient1 = new TcpClient();
                    mTcpClient1.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient1);

                    mTcpClient2 = new TcpClient();
                    mTcpClient2.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient2);

                    mTcpClient3 = new TcpClient();
                    mTcpClient3.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient);
                    mTcpClient4 = new TcpClient();
                    mTcpClient4.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient4);

                    mTcpClient5 = new TcpClient();
                    mTcpClient5.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient5);

                    mTcpClient6 = new TcpClient();
                    mTcpClient6.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient6);
                    mTcpClient7 = new TcpClient();
                    mTcpClient7.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient7);

                    mTcpClient8 = new TcpClient();
                    mTcpClient8.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient8);

                    mTcpClient9 = new TcpClient();
                    mTcpClient9.BeginConnect(ipa, nPort, onCompleteConnect, mTcpClient9);
                }
                catch(Exception ex)
                {

                }
            }
        }

        void onCompleteConnect(IAsyncResult iar)
        {
            TcpClient tcpc;

            
                tcpc = (TcpClient)iar.AsyncState;
               // tcpc.EndConnect(iar);
                mRx = new byte[512];

            //    tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, onCompleteReadFromServerStream, tcpc);



        }

        void onCompleteReadFromServerStream(IAsyncResult iar)
        {
            TcpClient tcpc;
            int nCountBytesReceivedFromServer;
            string strReceived;
            string textToSend = null ;
            
            try
            {
                tcpc = (TcpClient)iar.AsyncState;
                nCountBytesReceivedFromServer = tcpc.GetStream().EndRead(iar);

                if (nCountBytesReceivedFromServer == 0)
                {
                    MessageBox.Show("Connection broken.");
                    return;
                }
                strReceived = Encoding.ASCII.GetString(mRx, 0, nCountBytesReceivedFromServer);
                printLine(strReceived);
                string removeText;
                if (strReceived.Contains(" "))
                {
                     removeText = strReceived.Remove(0, strReceived.IndexOf(' ') + 1).ToUpper();

                }
                else
                {
                    removeText = strReceived;
                }
                if (recvIsGood(removeText))
                {
                    lastRecv = removeText;
                    Console.WriteLine(lastRecv);
                }
                if (strReceived.ToUpper()=="POLACZONO")
                {
                    Random log = new Random();
                    int login = 007;
                    textToSend = "LOGIN USER" + (char)login;
                }
                Console.WriteLine(strReceived);
                if (strReceived.ToUpper() == "TWOJ RUCH" || strReceived.ToUpper() == "POLACZONO")
                {
                    
                    if (lastRecv == null && strReceived.ToUpper() != "POLACZONO")
                    {
                        textToSend = getRandomCardOption();
                    }
                    else if(lastRecv != null && strReceived.ToUpper() != "POLACZONO" )
                    {
                        Random rnd = new Random();
                        int randomNumber = rnd.Next(0, 12);
                        if (randomNumber%3==0 && getValueOfCard(lastRecv)<380 && removeText != "CHECK")
                        {
                            textToSend = "CHECK";
                            lastRecv = null;
                        }
                        else if(getValueOfCard(lastRecv)>=380 && removeText != "CHECK")
                        {
                            textToSend = "CHECK";
                            lastRecv = null;
                        }
                        else
                        {
                            textToSend = getClientRequest(lastRecv);
                        }
                    }
                    byte[] tx;
                    if (string.IsNullOrEmpty(textToSend)) return;

                    try
                    {
                        tx = Encoding.ASCII.GetBytes(textToSend);

                        if (mTcpClient != null)
                        {
                            if (mTcpClient.Client.Connected)
                            {
                                printLine(textToSend);
                                mTcpClient.GetStream().BeginWrite(tx, 0, tx.Length, onCompleteWriteToServer, mTcpClient);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }

                }
                

                mRx = new byte[512];
                tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, onCompleteReadFromServerStream, tcpc);

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void printLine(string _strPrint)
        {
            tbConsole.Invoke(new Action<string>(doInvoke), _strPrint);
        }

        public void doInvoke(string _strPrint)
        {
            tbConsole.Text = _strPrint + Environment.NewLine + tbConsole.Text;
        }


        public void tbSend_Click(object sender, EventArgs e)
        {
            byte[] tx;
            
            if (string.IsNullOrEmpty(tbPayload.Text)) return;

            try
            {
                tx = Encoding.ASCII.GetBytes(tbPayload.Text);

                if (mTcpClient != null)
                {
                    if (mTcpClient.Client.Connected)
                    {
                        mTcpClient.GetStream().BeginWrite(tx, 0, tx.Length, onCompleteWriteToServer, mTcpClient);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
        void onCompleteWriteToServer(IAsyncResult iar)
        {
            TcpClient tcpc;

            try
            {
                tcpc = (TcpClient)iar.AsyncState;
                tcpc.GetStream().EndWrite(iar);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void tbConsole_TextChanged(object sender, EventArgs e)
        {

        }

        public string getClientRequest(string last)
        {
            string[] opportunitiesForChoice = {
	                            //Wysoka karta
	                            "V 2","V 3","V 4","V 5","V 6","V 7","V 8","V 9","V 10","V W","V D","V K","V A",
	                            //Para
	                            "R 2","R 3","R 4","R 5","R 6","R 7","R 8","R 9","R 10","R W","R D","R K","R A",
	                            //Dwie pary
	                            "W 2 3","W 2 4","W 2 5","W 2 6","W 2 7","W 2 8","W 2 9","W 2 10","W 2 W","W 2 D","W 2 K","W 2 A",
                                "W 3 2","W 3 4","W 3 5","W 3 6","W 3 7","W 3 8","W 3 9","W 3 10","W 3 W","W 3 D","W 3 K","W 3 A",
                                "W 4 2","W 4 3","W 4 5","W 4 6","W 4 7","W 4 8","W 4 9","W 4 10","W 4 W","W 4 D","W 4 K","W 4 A",
                                "W 5 2","W 5 3","W 5 4","W 5 6","W 5 7","W 5 8","W 5 9","W 5 10","W 5 W","W 5 D","W 5 K","W 5 A",
                                "W 6 2","W 6 3","W 6 4","W 6 5","W 6 7","W 6 8","W 6 9","W 6 10","W 6 W","W 6 D","W 6 K","W 6 A",
                                "W 7 2","W 7 3","W 7 4","W 7 5","W 7 6","W 7 8","W 7 9","W 7 10","W 7 W","W 7 D","W 7 K","W 7 A",
                                "W 8 2","W 8 3","W 8 4","W 8 5","W 8 6","W 8 7","W 8 9","W 8 10","W 8 W","W 8 D","W 8 K","W 8 A",
                                "W 9 2","W 9 3","W 9 4","W 9 5","W 9 6","W 9 7","W 9 8","W 9 10","W 9 W","W 9 D","W 9 K","W 9 A",
                                "W 10 2","W 10 3","W 10 4","W 10 5","W 10 6","W 10 7","W 10 8","W 10 9","W 10 W","W 10 D","W 10 K","W 10 A",
                                "W W 2","W W 3","W W 4","W W 5","W W 6","W W 7","W W 8","W W 9","W W 10","W W D","W W K","W W A",
                                "W D 2","W D 3","W D 4","W D 5","W D 6","W D 7","W D 8","W D 9","W D 10","W D W","W D K","W D A",
                                "W K 2","W K 3","W K 4","W K 5","W K 6","W K 7","W K 8","W K 9","W K 10","W K W","W K D","W K A",
                                "W A 2","W A 3","W A 4","W A 5","W A 6","W A 7","W A 8","W A 9","W A 10","W A W","W A D","W A K",
	                            //Trojka
	                            "T 2","T 3","T 4","T 5","T 6","T 7","T 8","T 9","T 10","T W","T D","T K","T A",
	                            //Strit
	                            "S 2","S 3","S 4","S 5","S 6","S 7","S 8","S 9","S 10",
	                            //Kolory
	                            "L C", "L D", "L H", "L S",
	                            //Full
	                            "F 2 3","F 2 4","F 2 5","F 2 6","F 2 7","F 2 8","F 2 9","F 2 10","F 2 F","F 2 D","F 2 K","F 2 A",
                                "F 3 2","F 3 4","F 3 5","F 3 6","F 3 7","F 3 8","F 3 9","F 3 10","F 3 F","F 3 D","F 3 K","F 3 A",
                                "F 4 3","F 4 2","F 4 5","F 4 6","F 4 7","F 4 8","F 4 9","F 4 10","F 4 F","F 4 D","F 4 K","F 4 A",
                                "F 5 3","F 5 4","F 5 2","F 5 6","F 5 7","F 5 8","F 5 9","F 5 10","F 5 F","F 5 D","F 5 K","F 5 A",
                                "F 6 3","F 6 4","F 6 5","F 6 2","F 6 7","F 6 8","F 6 9","F 6 10","F 6 F","F 6 D","F 6 K","F 6 A",
                                "F 7 3","F 7 4","F 7 5","F 7 6","F 7 2","F 7 8","F 7 9","F 7 10","F 7 F","F 7 D","F 7 K","F 7 A",
                                "F 8 3","F 8 4","F 8 5","F 8 6","F 8 7","F 8 2","F 8 9","F 8 10","F 8 F","F 8 D","F 8 K","F 8 A",
                                "F 9 3","F 9 4","F 9 5","F 9 6","F 9 7","F 9 8","F 9 2","F 9 10","F 9 F","F 9 D","F 9 K","F 9 A",
                                "F 10 3","F 10 4","F 10 5","F 10 6","F 10 7","F 10 8","F 10 9","F 10 2","F 10 F","F 10 D","F 10 K","F 10 A",
                                "F W 3","F W 4","F W 5","F W 6","F W 7","F W 8","F W 9","F W 10","F W 2","F W D","F W K","F W A",
                                "F D 3","F D 4","F D 5","F D 6","F D 7","F D 8","F D 9","F D 10","F D F","F D 2","F D K","F D A",
                                "F K 3","F K 4","F K 5","F K 6","F K 7","F K 8","F K 9","F K 10","F K F","F K D","F K 2","F K A",
                                "F A 3","F A 4","F A 5","F A 6","F A 7","F A 8","F A 9","F A 10","F A F","F A D","F A K","F A 2",
	                            //Kareta
	                            "K 2","K 3","K 4","K 5","K 6","K 7","K 8","K 9","K 10","K W","K D","K K","K A",
	                            //Poker
	                            "P 2C","P 3C","P 4C","P 5C","P 6C","P 7C","P 8C","P 9C","P 10C",
                                "P 2D","P 3D","P 4DD","P 5D","P 6D","P 7D","P 8D","P 9D","P 10D",
                                "P 2H","P 3H","P 4HH","P 5H","P 6H","P 7H","P 8H","P 9H","P 10H",
                                "P 2S","P 3S","P 4SS","P 5S","P 6S","P 7S","P 8S","P 9S","P 10S",
                           };
            int lastnumber = 0;
            for(int i=0;i<opportunitiesForChoice.Length;i++)
            {
                if(opportunitiesForChoice[i] == last)
                {
                    lastnumber = i;
                    break;
                }
            }
            Random rnd = new Random();
            int rndNumber = rnd.Next(1, 20);
            return opportunitiesForChoice[lastnumber + rndNumber];


        }

        public bool recvIsGood(string last)
        {
              string[] opportunitiesForChoice = {
	                            //Wysoka karta
	                            "V 2","V 3","V 4","V 5","V 6","V 7","V 8","V 9","V 10","V W","V D","V K","V A",
	                            //Para
	                            "R 2","R 3","R 4","R 5","R 6","R 7","R 8","R 9","R 10","R W","R D","R K","R A",
	                            //Dwie pary
	                            "W 2 3","W 2 4","W 2 5","W 2 6","W 2 7","W 2 8","W 2 9","W 2 10","W 2 W","W 2 D","W 2 K","W 2 A",
                                "W 3 2","W 3 4","W 3 5","W 3 6","W 3 7","W 3 8","W 3 9","W 3 10","W 3 W","W 3 D","W 3 K","W 3 A",
                                "W 4 2","W 4 3","W 4 5","W 4 6","W 4 7","W 4 8","W 4 9","W 4 10","W 4 W","W 4 D","W 4 K","W 4 A",
                                "W 5 2","W 5 3","W 5 4","W 5 6","W 5 7","W 5 8","W 5 9","W 5 10","W 5 W","W 5 D","W 5 K","W 5 A",
                                "W 6 2","W 6 3","W 6 4","W 6 5","W 6 7","W 6 8","W 6 9","W 6 10","W 6 W","W 6 D","W 6 K","W 6 A",
                                "W 7 2","W 7 3","W 7 4","W 7 5","W 7 6","W 7 8","W 7 9","W 7 10","W 7 W","W 7 D","W 7 K","W 7 A",
                                "W 8 2","W 8 3","W 8 4","W 8 5","W 8 6","W 8 7","W 8 9","W 8 10","W 8 W","W 8 D","W 8 K","W 8 A",
                                "W 9 2","W 9 3","W 9 4","W 9 5","W 9 6","W 9 7","W 9 8","W 9 10","W 9 W","W 9 D","W 9 K","W 9 A",
                                "W 10 2","W 10 3","W 10 4","W 10 5","W 10 6","W 10 7","W 10 8","W 10 9","W 10 W","W 10 D","W 10 K","W 10 A",
                                "W W 2","W W 3","W W 4","W W 5","W W 6","W W 7","W W 8","W W 9","W W 10","W W D","W W K","W W A",
                                "W D 2","W D 3","W D 4","W D 5","W D 6","W D 7","W D 8","W D 9","W D 10","W D W","W D K","W D A",
                                "W K 2","W K 3","W K 4","W K 5","W K 6","W K 7","W K 8","W K 9","W K 10","W K W","W K D","W K A",
                                "W A 2","W A 3","W A 4","W A 5","W A 6","W A 7","W A 8","W A 9","W A 10","W A W","W A D","W A K",
	                            //Trojka
	                            "T 2","T 3","T 4","T 5","T 6","T 7","T 8","T 9","T 10","T W","T D","T K","T A",
	                            //Strit
	                            "S 2","S 3","S 4","S 5","S 6","S 7","S 8","S 9","S 10",
	                            //Kolory
	                            "L C", "L D", "L H", "L S",
	                            //Full
	                            "F 2 3","F 2 4","F 2 5","F 2 6","F 2 7","F 2 8","F 2 9","F 2 10","F 2 F","F 2 D","F 2 K","F 2 A",
                                "F 3 2","F 3 4","F 3 5","F 3 6","F 3 7","F 3 8","F 3 9","F 3 10","F 3 F","F 3 D","F 3 K","F 3 A",
                                "F 4 3","F 4 2","F 4 5","F 4 6","F 4 7","F 4 8","F 4 9","F 4 10","F 4 F","F 4 D","F 4 K","F 4 A",
                                "F 5 3","F 5 4","F 5 2","F 5 6","F 5 7","F 5 8","F 5 9","F 5 10","F 5 F","F 5 D","F 5 K","F 5 A",
                                "F 6 3","F 6 4","F 6 5","F 6 2","F 6 7","F 6 8","F 6 9","F 6 10","F 6 F","F 6 D","F 6 K","F 6 A",
                                "F 7 3","F 7 4","F 7 5","F 7 6","F 7 2","F 7 8","F 7 9","F 7 10","F 7 F","F 7 D","F 7 K","F 7 A",
                                "F 8 3","F 8 4","F 8 5","F 8 6","F 8 7","F 8 2","F 8 9","F 8 10","F 8 F","F 8 D","F 8 K","F 8 A",
                                "F 9 3","F 9 4","F 9 5","F 9 6","F 9 7","F 9 8","F 9 2","F 9 10","F 9 F","F 9 D","F 9 K","F 9 A",
                                "F 10 3","F 10 4","F 10 5","F 10 6","F 10 7","F 10 8","F 10 9","F 10 2","F 10 F","F 10 D","F 10 K","F 10 A",
                                "F W 3","F W 4","F W 5","F W 6","F W 7","F W 8","F W 9","F W 10","F W 2","F W D","F W K","F W A",
                                "F D 3","F D 4","F D 5","F D 6","F D 7","F D 8","F D 9","F D 10","F D F","F D 2","F D K","F D A",
                                "F K 3","F K 4","F K 5","F K 6","F K 7","F K 8","F K 9","F K 10","F K F","F K D","F K 2","F K A",
                                "F A 3","F A 4","F A 5","F A 6","F A 7","F A 8","F A 9","F A 10","F A F","F A D","F A K","F A 2",
	                            //Kareta
	                            "K 2","K 3","K 4","K 5","K 6","K 7","K 8","K 9","K 10","K W","K D","K K","K A",
	                            //Poker
	                            "P 2C","P 3C","P 4C","P 5C","P 6C","P 7C","P 8C","P 9C","P 10C",
                                "P 2D","P 3D","P 4DD","P 5D","P 6D","P 7D","P 8D","P 9D","P 10D",
                                "P 2H","P 3H","P 4HH","P 5H","P 6H","P 7H","P 8H","P 9H","P 10H",
                                "P 2S","P 3S","P 4SS","P 5S","P 6S","P 7S","P 8S","P 9S","P 10S",
                           };
            for(int i=0;i<opportunitiesForChoice.Length;i++)
            {
                if(opportunitiesForChoice[i] == last)
                {
                    return true;
                }
            }
            return false;
        }

        public string getRandomCardOption()
        {
            string[] opportunitiesForChoice = {
	                            //Wysoka karta
	                            "V 2","V 3","V 4","V 5","V 6","V 7","V 8","V 9","V 10","V W","V D","V K","V A",
	                            //Para
	                            "R 2","R 3","R 4","R 5","R 6","R 7","R 8","R 9","R 10","R W","R D","R K","R A",
	                            //Dwie pary
	                            "W 2 3","W 2 4","W 2 5","W 2 6","W 2 7","W 2 8","W 2 9","W 2 10","W 2 W","W 2 D","W 2 K","W 2 A",
                                "W 3 2","W 3 4","W 3 5","W 3 6","W 3 7","W 3 8","W 3 9","W 3 10","W 3 W","W 3 D","W 3 K","W 3 A",
                                "W 4 2","W 4 3","W 4 5","W 4 6","W 4 7","W 4 8","W 4 9","W 4 10","W 4 W","W 4 D","W 4 K","W 4 A",
                                "W 5 2","W 5 3","W 5 4","W 5 6","W 5 7","W 5 8","W 5 9","W 5 10","W 5 W","W 5 D","W 5 K","W 5 A",
                                "W 6 2","W 6 3","W 6 4","W 6 5","W 6 7","W 6 8","W 6 9","W 6 10","W 6 W","W 6 D","W 6 K","W 6 A",
                                "W 7 2","W 7 3","W 7 4","W 7 5","W 7 6","W 7 8","W 7 9","W 7 10","W 7 W","W 7 D","W 7 K","W 7 A",
                                "W 8 2","W 8 3","W 8 4","W 8 5","W 8 6","W 8 7","W 8 9","W 8 10","W 8 W","W 8 D","W 8 K","W 8 A",
                                "W 9 2","W 9 3","W 9 4","W 9 5","W 9 6","W 9 7","W 9 8","W 9 10","W 9 W","W 9 D","W 9 K","W 9 A",
                                "W 10 2","W 10 3","W 10 4","W 10 5","W 10 6","W 10 7","W 10 8","W 10 9","W 10 W","W 10 D","W 10 K","W 10 A",
                                "W W 2","W W 3","W W 4","W W 5","W W 6","W W 7","W W 8","W W 9","W W 10","W W D","W W K","W W A",
                                "W D 2","W D 3","W D 4","W D 5","W D 6","W D 7","W D 8","W D 9","W D 10","W D W","W D K","W D A",
                                "W K 2","W K 3","W K 4","W K 5","W K 6","W K 7","W K 8","W K 9","W K 10","W K W","W K D","W K A",
                                "W A 2","W A 3","W A 4","W A 5","W A 6","W A 7","W A 8","W A 9","W A 10","W A W","W A D","W A K",
	                            //Trojka
	                            "T 2","T 3","T 4","T 5","T 6","T 7","T 8","T 9","T 10","T W","T D","T K","T A",
	                            //Strit
	                            "S 2","S 3","S 4","S 5","S 6","S 7","S 8","S 9","S 10",
	                            //Kolory
	                            "L C", "L D", "L H", "L S",
	                            //Full
	                            "F 2 3","F 2 4","F 2 5","F 2 6","F 2 7","F 2 8","F 2 9","F 2 10","F 2 F","F 2 D","F 2 K","F 2 A",
                                "F 3 2","F 3 4","F 3 5","F 3 6","F 3 7","F 3 8","F 3 9","F 3 10","F 3 F","F 3 D","F 3 K","F 3 A",
                                "F 4 3","F 4 2","F 4 5","F 4 6","F 4 7","F 4 8","F 4 9","F 4 10","F 4 F","F 4 D","F 4 K","F 4 A",
                                "F 5 3","F 5 4","F 5 2","F 5 6","F 5 7","F 5 8","F 5 9","F 5 10","F 5 F","F 5 D","F 5 K","F 5 A",
                                "F 6 3","F 6 4","F 6 5","F 6 2","F 6 7","F 6 8","F 6 9","F 6 10","F 6 F","F 6 D","F 6 K","F 6 A",
                                "F 7 3","F 7 4","F 7 5","F 7 6","F 7 2","F 7 8","F 7 9","F 7 10","F 7 F","F 7 D","F 7 K","F 7 A",
                                "F 8 3","F 8 4","F 8 5","F 8 6","F 8 7","F 8 2","F 8 9","F 8 10","F 8 F","F 8 D","F 8 K","F 8 A",
                                "F 9 3","F 9 4","F 9 5","F 9 6","F 9 7","F 9 8","F 9 2","F 9 10","F 9 F","F 9 D","F 9 K","F 9 A",
                                "F 10 3","F 10 4","F 10 5","F 10 6","F 10 7","F 10 8","F 10 9","F 10 2","F 10 F","F 10 D","F 10 K","F 10 A",
                                "F W 3","F W 4","F W 5","F W 6","F W 7","F W 8","F W 9","F W 10","F W 2","F W D","F W K","F W A",
                                "F D 3","F D 4","F D 5","F D 6","F D 7","F D 8","F D 9","F D 10","F D F","F D 2","F D K","F D A",
                                "F K 3","F K 4","F K 5","F K 6","F K 7","F K 8","F K 9","F K 10","F K F","F K D","F K 2","F K A",
                                "F A 3","F A 4","F A 5","F A 6","F A 7","F A 8","F A 9","F A 10","F A F","F A D","F A K","F A 2",
	                            //Kareta
	                            "K 2","K 3","K 4","K 5","K 6","K 7","K 8","K 9","K 10","K W","K D","K K","K A",
	                            //Poker
	                            "P 2C","P 3C","P 4C","P 5C","P 6C","P 7C","P 8C","P 9C","P 10C",
                                "P 2D","P 3D","P 4DD","P 5D","P 6D","P 7D","P 8D","P 9D","P 10D",
                                "P 2H","P 3H","P 4HH","P 5H","P 6H","P 7H","P 8H","P 9H","P 10H",
                                "P 2S","P 3S","P 4SS","P 5S","P 6S","P 7S","P 8S","P 9S","P 10S",
                           };
            Random rnd = new Random();
            int randomnumber = rnd.Next(0, 200);
            return opportunitiesForChoice[randomnumber];
        }

        public int getValueOfCard(string text)
        {
            string[] opportunitiesForChoice = {
	                            //Wysoka karta
	                            "V 2","V 3","V 4","V 5","V 6","V 7","V 8","V 9","V 10","V W","V D","V K","V A",
	                            //Para
	                            "R 2","R 3","R 4","R 5","R 6","R 7","R 8","R 9","R 10","R W","R D","R K","R A",
	                            //Dwie pary
	                            "W 2 3","W 2 4","W 2 5","W 2 6","W 2 7","W 2 8","W 2 9","W 2 10","W 2 W","W 2 D","W 2 K","W 2 A",
                                "W 3 2","W 3 4","W 3 5","W 3 6","W 3 7","W 3 8","W 3 9","W 3 10","W 3 W","W 3 D","W 3 K","W 3 A",
                                "W 4 2","W 4 3","W 4 5","W 4 6","W 4 7","W 4 8","W 4 9","W 4 10","W 4 W","W 4 D","W 4 K","W 4 A",
                                "W 5 2","W 5 3","W 5 4","W 5 6","W 5 7","W 5 8","W 5 9","W 5 10","W 5 W","W 5 D","W 5 K","W 5 A",
                                "W 6 2","W 6 3","W 6 4","W 6 5","W 6 7","W 6 8","W 6 9","W 6 10","W 6 W","W 6 D","W 6 K","W 6 A",
                                "W 7 2","W 7 3","W 7 4","W 7 5","W 7 6","W 7 8","W 7 9","W 7 10","W 7 W","W 7 D","W 7 K","W 7 A",
                                "W 8 2","W 8 3","W 8 4","W 8 5","W 8 6","W 8 7","W 8 9","W 8 10","W 8 W","W 8 D","W 8 K","W 8 A",
                                "W 9 2","W 9 3","W 9 4","W 9 5","W 9 6","W 9 7","W 9 8","W 9 10","W 9 W","W 9 D","W 9 K","W 9 A",
                                "W 10 2","W 10 3","W 10 4","W 10 5","W 10 6","W 10 7","W 10 8","W 10 9","W 10 W","W 10 D","W 10 K","W 10 A",
                                "W W 2","W W 3","W W 4","W W 5","W W 6","W W 7","W W 8","W W 9","W W 10","W W D","W W K","W W A",
                                "W D 2","W D 3","W D 4","W D 5","W D 6","W D 7","W D 8","W D 9","W D 10","W D W","W D K","W D A",
                                "W K 2","W K 3","W K 4","W K 5","W K 6","W K 7","W K 8","W K 9","W K 10","W K W","W K D","W K A",
                                "W A 2","W A 3","W A 4","W A 5","W A 6","W A 7","W A 8","W A 9","W A 10","W A W","W A D","W A K",
	                            //Trojka
	                            "T 2","T 3","T 4","T 5","T 6","T 7","T 8","T 9","T 10","T W","T D","T K","T A",
	                            //Strit
	                            "S 2","S 3","S 4","S 5","S 6","S 7","S 8","S 9","S 10",
	                            //Kolory
	                            "L C", "L D", "L H", "L S",
	                            //Full
	                            "F 2 3","F 2 4","F 2 5","F 2 6","F 2 7","F 2 8","F 2 9","F 2 10","F 2 F","F 2 D","F 2 K","F 2 A",
                                "F 3 2","F 3 4","F 3 5","F 3 6","F 3 7","F 3 8","F 3 9","F 3 10","F 3 F","F 3 D","F 3 K","F 3 A",
                                "F 4 3","F 4 2","F 4 5","F 4 6","F 4 7","F 4 8","F 4 9","F 4 10","F 4 F","F 4 D","F 4 K","F 4 A",
                                "F 5 3","F 5 4","F 5 2","F 5 6","F 5 7","F 5 8","F 5 9","F 5 10","F 5 F","F 5 D","F 5 K","F 5 A",
                                "F 6 3","F 6 4","F 6 5","F 6 2","F 6 7","F 6 8","F 6 9","F 6 10","F 6 F","F 6 D","F 6 K","F 6 A",
                                "F 7 3","F 7 4","F 7 5","F 7 6","F 7 2","F 7 8","F 7 9","F 7 10","F 7 F","F 7 D","F 7 K","F 7 A",
                                "F 8 3","F 8 4","F 8 5","F 8 6","F 8 7","F 8 2","F 8 9","F 8 10","F 8 F","F 8 D","F 8 K","F 8 A",
                                "F 9 3","F 9 4","F 9 5","F 9 6","F 9 7","F 9 8","F 9 2","F 9 10","F 9 F","F 9 D","F 9 K","F 9 A",
                                "F 10 3","F 10 4","F 10 5","F 10 6","F 10 7","F 10 8","F 10 9","F 10 2","F 10 F","F 10 D","F 10 K","F 10 A",
                                "F W 3","F W 4","F W 5","F W 6","F W 7","F W 8","F W 9","F W 10","F W 2","F W D","F W K","F W A",
                                "F D 3","F D 4","F D 5","F D 6","F D 7","F D 8","F D 9","F D 10","F D F","F D 2","F D K","F D A",
                                "F K 3","F K 4","F K 5","F K 6","F K 7","F K 8","F K 9","F K 10","F K F","F K D","F K 2","F K A",
                                "F A 3","F A 4","F A 5","F A 6","F A 7","F A 8","F A 9","F A 10","F A F","F A D","F A K","F A 2",
	                            //Kareta
	                            "K 2","K 3","K 4","K 5","K 6","K 7","K 8","K 9","K 10","K W","K D","K K","K A",
	                            //Poker
	                            "P 2C","P 3C","P 4C","P 5C","P 6C","P 7C","P 8C","P 9C","P 10C",
                                "P 2D","P 3D","P 4DD","P 5D","P 6D","P 7D","P 8D","P 9D","P 10D",
                                "P 2H","P 3H","P 4HH","P 5H","P 6H","P 7H","P 8H","P 9H","P 10H",
                                "P 2S","P 3S","P 4SS","P 5S","P 6S","P 7S","P 8S","P 9S","P 10S",
                           };
            for(int i=0;i<opportunitiesForChoice.Length;i++)
            {
                if(text == opportunitiesForChoice[i])
                {
                    return i;
                   
                }
            }
            return 0;
        }
    }
}
