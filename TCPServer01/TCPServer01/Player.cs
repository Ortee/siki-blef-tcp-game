﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCPServer01
{
    class Player
    {
        int ID;
        string ip;
        int points;
        string name;
        bool onTheGame;
        bool yourRound;
        bool acceptedToGame;
        public Player(string nick, string ipek,int idek)
        {
            ID = idek;
            acceptedToGame = false;
            yourRound = false;
            name = nick;
            ip = ipek;
            points = 0;
            onTheGame = true;
        }
        public bool getAcceptedToGame()
        {
            return acceptedToGame;
        }
        public void setAcceptedToGame(bool tru)
        {
            acceptedToGame = tru;
        }
        public bool getYourRound()
        {
            return yourRound;
        }
        public void setYourRound(bool r)
        {
            yourRound = r;
        }
        public bool getOnTheGame()
        {
            return onTheGame;
        }
        public void setOnTheGame(bool y)
        {
            onTheGame = y;
        }
        public string getName()
        {
            return name;
        }
        public void setName(string nam)
        {
            name = nam;
        }
        public string getIP()
        {
            return ip;
        }

        public int getPoints()
        {
            return points;
        }

        public void AddOnePoint()
        {
            points++;
        }
        public int getID()
        {

            return ID;
        }


    }
}
