﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCPServer01
{
    class Card
    {
        string[] values = new string[] { "2", "3", "4", "5", "6", "7", "8", "9", "10" , "W", "D", "K", "A"};
        string[] colors = new string[] { "S", "H", "D", "C" };
        string value;
        string color;
        string playerIp;
        public Card(int random_value,int random_color, string ip)
        {
            playerIp = ip;
            value = values[random_value];
            color = colors[random_color];
        }

        public string getPlayerIp()
        {
            return playerIp;
        }

        public char getValue()
        {
            return value[0];
        }
        
        public char getColor()
        {
            return color[0];
        }

        public string getCardColorAndValue()
        {
            return value + color;
        }

        //potrzeby sprawdzania na sucho

        public void setValue(string var)
        {
            value = var;
        }

        public void setColor(string col)
        {
            color = col;
        }
    }
}
