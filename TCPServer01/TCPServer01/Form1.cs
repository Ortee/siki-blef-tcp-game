﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Collections;

namespace TCPServer01
{
    public partial class Form1 : Form
    {

        TcpListener mTCPListener;
        TcpClient mTCPClient;
        //CFG
        int playerCount = 5;
        int playerTime = 3;
        int startPlayerID = 0;
        int roundNumber = 0;
        Player beforeSender=null;
        /// 
        public int readyPlayers = 0;
        public bool gameStarted = false;
        public bool firstCheck = false;
        List<string> losePlayers = new List<string>();
        List<Player> startGamers;
        byte[] mRx;
        string lastPlayerSay;
        int indexPlayer = 0;
        int errormsg = 0;
        Stack stack = new Stack();
        public static int timerTime = 1;
        bool yourRound = false;

        private List<ClientNode> mlClientSocks;
        public System.IO.StreamWriter file;
        public string[] opportunitiesForChoice = {
	                            //Wysoka karta
	                            "V 2","V 3","V 4","V 5","V 6","V 7","V 8","V 9","V 10","V W","V D","V K","V A",
	                            //Para
	                            "R 2","R 3","R 4","R 5","R 6","R 7","R 8","R 9","R 10","R W","R D","R K","R A",
	                            //Dwie pary
	                            "W 2 3","W 2 4","W 2 5","W 2 6","W 2 7","W 2 8","W 2 9","W 2 10","W 2 W","W 2 D","W 2 K","W 2 A",
                                "W 3 2","W 3 4","W 3 5","W 3 6","W 3 7","W 3 8","W 3 9","W 3 10","W 3 W","W 3 D","W 3 K","W 3 A",
                                "W 4 2","W 4 3","W 4 5","W 4 6","W 4 7","W 4 8","W 4 9","W 4 10","W 4 W","W 4 D","W 4 K","W 4 A",
                                "W 5 2","W 5 3","W 5 4","W 5 6","W 5 7","W 5 8","W 5 9","W 5 10","W 5 W","W 5 D","W 5 K","W 5 A",
                                "W 6 2","W 6 3","W 6 4","W 6 5","W 6 7","W 6 8","W 6 9","W 6 10","W 6 W","W 6 D","W 6 K","W 6 A",
                                "W 7 2","W 7 3","W 7 4","W 7 5","W 7 6","W 7 8","W 7 9","W 7 10","W 7 W","W 7 D","W 7 K","W 7 A",
                                "W 8 2","W 8 3","W 8 4","W 8 5","W 8 6","W 8 7","W 8 9","W 8 10","W 8 W","W 8 D","W 8 K","W 8 A",
                                "W 9 2","W 9 3","W 9 4","W 9 5","W 9 6","W 9 7","W 9 8","W 9 10","W 9 W","W 9 D","W 9 K","W 9 A",
                                "W 10 2","W 10 3","W 10 4","W 10 5","W 10 6","W 10 7","W 10 8","W 10 9","W 10 W","W 10 D","W 10 K","W 10 A",
                                "W W 2","W W 3","W W 4","W W 5","W W 6","W W 7","W W 8","W W 9","W W 10","W W D","W W K","W W A",
                                "W D 2","W D 3","W D 4","W D 5","W D 6","W D 7","W D 8","W D 9","W D 10","W D W","W D K","W D A",
                                "W K 2","W K 3","W K 4","W K 5","W K 6","W K 7","W K 8","W K 9","W K 10","W K W","W K D","W K A",
                                "W A 2","W A 3","W A 4","W A 5","W A 6","W A 7","W A 8","W A 9","W A 10","W A W","W A D","W A K",
	                            //Trojka
	                            "T 2","T 3","T 4","T 5","T 6","T 7","T 8","T 9","T 10","T W","T D","T K","T A",
	                            //Strit
	                            "S 2","S 3","S 4","S 5","S 6","S 7","S 8","S 9","S 10",
	                            //Kolory
	                            "L C", "L D", "L H", "L S",
	                            //Full
	                            "F 2 3","F 2 4","F 2 5","F 2 6","F 2 7","F 2 8","F 2 9","F 2 10","F 2 W","F 2 D","F 2 K","F 2 A",
                                "F 3 2","F 3 4","F 3 5","F 3 6","F 3 7","F 3 8","F 3 9","F 3 10","F 3 W","F 3 D","F 3 K","F 3 A",
                                "F 4 2","F 4 3","F 4 5","F 4 6","F 4 7","F 4 8","F 4 9","F 4 10","F 4 W","F 4 D","F 4 K","F 4 A",
                                "F 5 2","F 5 3","F 5 4","F 5 6","F 5 7","F 5 8","F 5 9","F 5 10","F 5 W","F 5 D","F 5 K","F 5 A",
                                "F 6 2","F 6 3","F 6 4","F 6 5","F 6 7","F 6 8","F 6 9","F 6 10","F 6 W","F 6 D","F 6 K","F 6 A",
                                "F 7 3","F 7 4","F 7 5","F 7 6","F 7 2","F 7 8","F 7 9","F 7 10","F 7 W","F 7 D","F 7 K","F 7 A",
                                "F 8 2","F 8 3","F 8 4","F 8 5","F 8 6","F 8 7","F 8 9","F 8 10","F 8 W","F 8 D","F 8 K","F 8 A",
                                "F 9 2","F 9 3","F 9 4","F 9 5","F 9 6","F 9 7","F 9 8","F 9 10","F 9 W","F 9 D","F 9 K","F 9 A",
                                "F 10 2","F 10 3","F 10 4","F 10 5","F 10 6","F 10 7","F 10 8","F 10 9","F 10 F","F 10 D","F 10 K","F 10 A",
                                "F W 2","F W 3","F W 4","F W 5","F W 6","F W 7","F W 8","F W 9","F W 10","F W D","F W K","F W A",
                                "F D 2","F D 3","F D 4","F D 5","F D 6","F D 7","F D 8","F D 9","F D 10","F D F","F D K","F D A",
                                "F K 2","F K 3","F K 4","F K 5","F K 6","F K 7","F K 8","F K 9","F K 10","F K F","F K D","F K A",
                                "F A 2","F A 3","F A 4","F A 5","F A 6","F A 7","F A 8","F A 9","F A 10","F A F","F A D","F A K",
	                            //Kareta
	                            "K 2","K 3","K 4","K 5","K 6","K 7","K 8","K 9","K 10","K W","K D","K K","K A",
	                            //Poker
	                            "P 2C","P 3C","P 4C","P 5C","P 6C","P 7C","P 8C","P 9C","P 10C",
                                "P 2D","P 3D","P 4DD","P 5D","P 6D","P 7D","P 8D","P 9D","P 10D",
                                "P 2H","P 3H","P 4HH","P 5H","P 6H","P 7H","P 8H","P 9H","P 10H",
                                "P 2S","P 3S","P 4SS","P 5S","P 6S","P 7S","P 8S","P 9S","P 10S",
                           };
        public Form1()
        {
            file = new System.IO.StreamWriter("./log.txt");

            startGamers = new List<Player>(10);

            InitializeComponent();
            mlClientSocks = new List<ClientNode>(10);
            CheckForIllegalCrossThreadCalls = false;
            label2.Text = "SERWER JEST PRZYSTOSOWANY NA: " + playerCount.ToString() + " GRACZY";

        }

        IPAddress findMyIPV4Address()
        {
            string strThisHostName = string.Empty;
            IPHostEntry thisHostDNSEntry = null;
            IPAddress[] allIPsOfThisHost = null;
            IPAddress ipv4Ret = null;

            try
            {
                strThisHostName = System.Net.Dns.GetHostName();
                thisHostDNSEntry = System.Net.Dns.GetHostEntry(strThisHostName);
                allIPsOfThisHost = thisHostDNSEntry.AddressList;
                int lengthB = allIPsOfThisHost.Length;
                lengthB = lengthB - 1;
                for (int idx = lengthB; idx >= 0; idx--)
                {
                    if (allIPsOfThisHost[idx].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipv4Ret = allIPsOfThisHost[idx];
                        break;
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            return ipv4Ret;
        }
        private void btnStartListening_Click(object sender, EventArgs e)
        {
            IPAddress ipaddr;
            int nPort;

            if (!int.TryParse(tbPort.Text, out nPort))
            {
                nPort = 23000;
            }
            if (!IPAddress.TryParse(tbIPAddress.Text, out ipaddr))
            {
                MessageBox.Show("Invalid IP address supplied.");
                return;
            }

            mTCPListener = new TcpListener(ipaddr, nPort);

            mTCPListener.Start();

            mTCPListener.BeginAcceptTcpClient(onCompleteAcceptTcpClient, mTCPListener);


        }
        List<Player> PlayerL = new List<Player>();
        public void AddPlayer(string name, string ip,int idek)
        {

            PlayerL.Add(new Player(name, ip,idek));
        }
        void onCompleteAcceptTcpClient(IAsyncResult iar)
        {
            TcpListener tcpl = (TcpListener)iar.AsyncState;
            TcpClient tclient = null;
            ClientNode cNode = null;

            try
            {
                tclient = tcpl.EndAcceptTcpClient(iar);

                printLine("[" + DateTime.Now + "]"+"Client Connected...");


                tcpl.BeginAcceptTcpClient(onCompleteAcceptTcpClient, tcpl);

                lock (mlClientSocks)
                {
                    mlClientSocks.Add((cNode = new ClientNode(tclient, new byte[512], new byte[512], tclient.Client.RemoteEndPoint.ToString())));
                    if (PlayerL.Count < playerCount)
                    {
                        if (!ipUsed(cNode.ToString()))
                        {

                            lbClients.Items.Add(cNode.ToString());
                            sendMsgToPlayer(cNode.ToString(), "POLACZONO");
                            PlayerL.Add(new Player("DEFAULTNAME", cNode.ToString(), lbClients.Items.Count));
                            if (lbClients.Items.Count == playerCount)
                            {
                                timer1.Enabled = true;
                                Random rnd2 = new Random();
                                startPlayerID = rnd2.Next(1, playerCount);
                            }
                        }
                    }

                }
                tclient.GetStream().BeginRead(cNode.Rx, 0, cNode.Rx.Length, onCompleteReadFromTCPClientStream, tclient);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error12", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void onCompleteReadFromTCPClientStream(IAsyncResult iar)
        {
            TcpClient tcpc;

            int nCountReadBytes = 0;
            string strRecv;
            ClientNode cn = null;

            try
            {
                lock (mlClientSocks)
                {
                    tcpc = (TcpClient)iar.AsyncState;

                    cn = mlClientSocks.Find(x => x.strId == tcpc.Client.RemoteEndPoint.ToString());

                    nCountReadBytes = tcpc.GetStream().EndRead(iar);
                    Player sender = null;
                    foreach (Player p in PlayerL)
                    {
                        if (p.getIP() == cn.ToString())
                        {
                            sender = p;
                        }
                    }
                    if (!sender.Equals(null))
                    {
                        if (nCountReadBytes == 0)
                        {
                            MessageBox.Show("Client disconnected.");
                            mlClientSocks.Remove(cn);
                            PlayerL.Remove(sender);
                            playerCount--;
                            lbClients.Items.Remove(cn.ToString());

                        }
                        strRecv = Encoding.ASCII.GetString(cn.Rx, 0, nCountReadBytes).Trim();
                        if (this.getGameStarted() == false)
                        {
                            if (strRecv.Substring(0, 5) == "LOGIN")
                            {
                                sender.setName(strRecv.Remove(0, strRecv.IndexOf(' ') + 1).ToUpper());
                                this.setNextReadyPlayer();
                                sender.setAcceptedToGame(true);
                            }

                            if (PlayerL.Count == playerCount)
                            {
                                for (int j = 0; j < PlayerL.Count; j++)
                                {
                                    if (PlayerL[j] == sender)
                                    {
                                        indexPlayer = j;
                                    }
                                }
                            }
                        }
                        if (this.getGameStarted() == false && this.getReadyPlayers() == playerCount)
                        {
                            startGamers = PlayerL;
                            this.setGameStarted(true);
                            sendMsgToAllPlayer(lbClients.Items.Cast<String>().ToArray(), "START ");
                            timerTime = 1;
                            first_round(PlayerL);
                        }


                        if (sender.getYourRound() == true && this.getGameStarted() == true)
                        {

                            printLine("[" + DateTime.Now + "]" + " MSG FROM PLAYER :"+ " - " + sender.getName().ToString() + ": " + strRecv);
                            try {
                                if (strRecv.ToUpper() == "CHECK" && firstCheck == true)
                                {
                                    firstCheck = false;
                                    roundNumber++;
                                        errormsg = 0;
                                    try
                                    {
                                        if (playerChooseCHECK(lastPlayerSay))
                                        {
                                            sendMsgToPlayer(sender.getIP().ToString(), "SUKCES");
                                            beforeSender.AddOnePoint();
                                        }
                                        else
                                        {
                               
                                            sendMsgToPlayer(sender.getIP().ToString(), "PORAZKA");
                                            sender.AddOnePoint();

                                        }
                                    }
                                    catch (NullReferenceException ex)
                                    {
                                        Console.WriteLine(lastPlayerSay);
                                        if(beforeSender==null)
                                        {
                                            MessageBox.Show("2ERROR WITH ROUND PLAYER ID");


                                        }
                                        if (sender == null)
                                        {
                                            MessageBox.Show("ERROR WITH ROUND PLAYER ID");

                                        }

                                    }
                                    string newRoundText;
                                        List<string> lst = lbClients.Items.Cast<String>().ToList();
                                        lst.Remove(sender.getIP());
                                        sendMsgToAllPlayer(lst.ToArray(), sender.getID().ToString() + " " + strRecv);
                                        newRoundText = PlayerL.Count.ToString() + " ";
                                        foreach (Player p in PlayerL)
                                        {
                                            newRoundText = newRoundText + p.getID().ToString() + " " + p.getPoints().ToString() + " ";

                                            newRoundText = newRoundText + getPlayerCardsFromStack(p.getIP());
                                        }
                                        sendMsgToAllPlayer(lbClients.Items.Cast<String>().ToArray(), newRoundText );
                                        printLine("[" + DateTime.Now + "]"+newRoundText );
                                        newRoundText = PlayerL.Count.ToString() + " ";
                                        foreach (Player p in PlayerL)
                                        {
                                            newRoundText = newRoundText + p.getID().ToString() + " " + (p.getPoints()+1).ToString() + " ";
                                        }
                                        sendMsgToAllPlayer(lbClients.Items.Cast<String>().ToArray(), newRoundText );
                                    clearStack();
                                    try
                                    {
                                        if (checkPoints() || playerCount == 1)
                                        {
                                            
                                                outPlayerFromGame();
                                                if (playerCount <= 1)
                                                {
                                                    endOfGame();
                                                }
                                        }
                                           
                                     }
                                    catch (NullReferenceException ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error5", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    try
                                    {
                                        if ((startPlayerID-1) > 0)
                                        {
                                            sendMsgToAllPlayer(lbClients.Items.Cast<String>().ToArray(), "RUNDA " + roundNumber.ToString() + " " + PlayerL[startPlayerID - 1].getID().ToString() );
                                        }
                                       
                                    }
                                    catch (System.ArgumentOutOfRangeException)
                                    {
                                      //  Console.WriteLine("ERROR WITH ROUND PLAYER ID");
                                      //  sendMsgToAllPlayer(lbClients.Items.Cast<String>().ToArray(), "RUNDA " + roundNumber.ToString() + " " + PlayerL[startPlayerID].getID().ToString() );

                                      //  MessageBox.Show("ERROR WITH ROUND PLAYER ID2");
                                    }
                                    first_round(PlayerL);
                                    
                                }
                            else
                            {
                                if (!playerSayGoodMsg(strRecv.ToUpper()))
                                {
                                    if(strRecv.ToUpper()!="CHECK")
                                    {
                                            errormsg++;
                                            printLine("SERWER: ERROR MSG!");
                                    }
                                    else
                                    {
                                            errormsg=0;
                                            printLine("SERWER: OK!");
                                     }
                                }
                                else
                                {
                                    if (playerSayGoodMsg(strRecv.ToUpper()))
                                    {
                                        printLine("SERWER: OK!");
                                        firstCheck = true;
                                        lastPlayerSay = strRecv;
                                        List<string> lst = lbClients.Items.Cast<String>().ToList();
                                        lst.Remove(sender.getIP());
                                        sendMsgToAllPlayer(lst.ToArray(), sender.getID().ToString() + " " + strRecv);
                                        beforeSender = sender;
                                    }
                                    errormsg = 0;
                                }
                                if (errormsg >= 100)
                                {
                                    //MessageBox.Show("Kick client: " + sender.getName());
                                    mlClientSocks.Remove(cn);
                                    PlayerL.Remove(sender);
                                    playerCount--;
                                    lbClients.Items.Remove(cn.ToString());
                                }
                            }
                        }
                            catch (NullReferenceException ex)
                            {
                                MessageBox.Show(ex.Message, "Error2", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        cn.Rx = new byte[512];

                        tcpc.GetStream().BeginRead(cn.Rx, 0, cn.Rx.Length, onCompleteReadFromTCPClientStream, tcpc);

                    }
                }

                }
            catch (Exception ex)
            {

                    foreach(Player p in PlayerL)
                    {
                        if(p.getIP()==cn.ToString())
                        {
                            PlayerL.Remove(p);
                            break;
                        }
                    }
                    playerCount--;
                    printLine("[" + DateTime.Now + "]"+"Client disconnected: " + cn.ToString());
                    mlClientSocks.Remove(cn);
                    lbClients.Items.Remove(cn.ToString());
                

            }
        }
        public void printLine(string _strPrint)
        {
            file.WriteLine(_strPrint);
            tbConsoleOutput.Invoke(new Action<string>(doInvoke), _strPrint.Replace(@"[" + DateTime.Now + "]",""));
        }
        public void doInvoke(string _strPrint)
        {
            tbConsoleOutput.Text = _strPrint + Environment.NewLine + tbConsoleOutput.Text;
        }
        public void sendMsgToPlayer(string clientIP, string msg)
        {
            printLine("["+DateTime.Now+"]" + " SEND MSG TO: " +clientIP+" :"+msg);
            ClientNode cn = null;
            lock (mlClientSocks)
            {
                

                try
                {
                    cn = mlClientSocks.Find(x => x.strId == clientIP);
                    cn.Tx = new byte[512];
                    if (cn != null)
                    {
                        if (cn.tclient != null)
                        {
                            if (cn.tclient.Client.Connected)
                            {
                                cn.Tx = Encoding.ASCII.GetBytes(msg);
                                cn.tclient.GetStream().BeginWrite(cn.Tx, 0, cn.Tx.Length, onCompleteWriteToClientStream, cn.tclient);
                            }
                        }
                    }
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    //MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void sendMsgToAllPlayer(Array clientIPs, string msg)
        {
            printLine("[" + DateTime.Now + "]" + " SEND MSG TO ALL PLAYERS: " + msg);

            int id = 1;
            foreach (string clientIP in clientIPs)
            {
                ClientNode cn = null;

                lock (mlClientSocks)
                {
                    cn = mlClientSocks.Find(x => x.strId == clientIP);
                    cn.Tx = new byte[512];

                    try
                    {
                        if (cn != null)
                        {
                            if (cn.tclient != null)
                            {
                                if (cn.tclient.Client.Connected)
                                {

                                    if (msg == "START ")
                                    {
                                        cn.Tx = Encoding.ASCII.GetBytes(msg + id + " " + startPlayerID.ToString());
                                    }
                                    else
                                    {
                                        cn.Tx = Encoding.ASCII.GetBytes(msg);
                                    }
                                    cn.tclient.GetStream().BeginWrite(cn.Tx, 0, cn.Tx.Length, onCompleteWriteToClientStream, cn.tclient);
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                id++;
            }
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (lbClients.Items.Count <= 0) return;
            if (string.IsNullOrEmpty(tbPayload.Text)) return;

            ClientNode cn = null;

            lock (mlClientSocks)
            {
                cn = mlClientSocks.Find(x => x.strId == lbClients.SelectedItem.ToString());
                cn.Tx = new byte[512];

                try
                {
                    if (cn != null)
                    {
                        if (cn.tclient != null)
                        {
                            if (cn.tclient.Client.Connected)
                            {
                                cn.Tx = Encoding.ASCII.GetBytes(tbPayload.Text);
                                cn.tclient.GetStream().BeginWrite(cn.Tx, 0, cn.Tx.Length, onCompleteWriteToClientStream, cn.tclient);
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void onCompleteWriteToClientStream(IAsyncResult iar)
        {
            try
            {
                TcpClient tcpc = (TcpClient)iar.AsyncState;
                tcpc.GetStream().EndWrite(iar);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnFindIPv4IP_Click(object sender, EventArgs e)
        {
            IPAddress ipa = null;
            ipa = findMyIPV4Address();

            if (ipa != null)
            {
                tbIPAddress.Text = ipa.ToString();
            }
        }
        private void btnFindIPv4IP_Click_1(object sender, EventArgs e)
        {
            IPAddress ipa = null;

            ipa = findMyIPV4Address();
            if (ipa != null)
            {
                tbIPAddress.Text = ipa.ToString();
            }
        }
        private void tbIPAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private bool checkPoints()
        {
            foreach (Player p in PlayerL)
            {
                if (p.getPoints() == 5 && p.getOnTheGame()==true)
                {
                    losePlayers.Add(p.getName());
                    return true;
                }

            }
            return false;
        }
        private Card getCard(int random_value, int random_color, string playerIp)
        {
            Card card = new Card(random_value, random_color, playerIp);
            Random rnd = new Random();
            while (this.checkStack(card) == true)
            {
                random_value = rnd.Next(0, 12);
                random_color = rnd.Next(0, 3);
                card = new Card(random_value, random_color, playerIp);
            }
            return card;
        }

        private void first_round(List<Player> playersL)
        {

            int iteration = 0;
            Random rnd = new Random();
            int random_value, random_color;
            foreach (Player p in playersL)
            {
                random_value = rnd.Next(0, 12);
                random_color = rnd.Next(0, 3);
                sendMsgToPlayer(p.getIP(), (p.getPoints() + 1).ToString());
                for (int i = 0; i < p.getPoints() + 1; i++)
                {
                    sendMsgToPlayer(p.getIP(), " " + this.getCard(random_value, random_color, p.getIP()).getCardColorAndValue());
                    this.addToStack(this.getCard(random_value, random_color, p.getIP()));
                }

                iteration++;
            }
            //this.showStack(); // POKAZUJE ROZDAWANE KARTY

        }
        string[] points = new string[10];

        public bool playerSayGoodMsg(string text)
        {

            for (int i = 0; i < opportunitiesForChoice.Length; i++)
            {
                if (text == opportunitiesForChoice[i])
                {
                    return true;
                }
            }
            return false;
        }
        public bool playerChooseCHECK(string text)
        {
            string[] values = text.Split(' ');
            
            int intChoose = 0;
            for (int i = 0; i < opportunitiesForChoice.Length; i++)
            {

                if (opportunitiesForChoice[i] == text)
                {
                    intChoose = i;
                }
            }
            if (intChoose >= 0 && intChoose <= 25 || intChoose >= 182 && intChoose <= 207 || intChoose >= 364 && intChoose <= 376)
            {

                if (playerSayCheckOneValue(values[0][0], values[1]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (intChoose >= 26 && intChoose <= 181 || intChoose >= 208 && intChoose <= 363)
            {
                if (playerSayCheckTwoValues(values[0][0], values[1], values[2]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (intChoose >= 377 && intChoose <= 412)
            {
                if (values[1][0] != '1')
                {
                    if (playerSayCheckPOKER(values[0][0], values[1][0].ToString(), values[1][1]))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (playerSayCheckPOKER(values[0][0], values[1][0].ToString() + values[1][1].ToString(), values[1][2]))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }
        public bool playerSayCheckPOKER(char choose, string value, char color)
        {
            if (value == "10")
            {
                value = "T";
            }


            switch (choose)
            {
                case 'P':
                    printLine("Poker");
                    if (cardCheckPoker(value, color))
                    {
                        printLine("SERWER: JEST POKER WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA POKERA WSROD ZAWODNIKOW!");
                        return false;
                    }

            }
            return false;

        }
        public bool playerSayCheckOneValue(char choose, string value)
        {
            if (value == "10")
            {
                value = "T";
            }


            switch (choose)
            {

                case 'K':
                    if (cardCheckFourOfaKind(value))
                    {
                        printLine("SERWER: JEST KARETA WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA KARETY WSROD ZAWODNIKOW!");
                        return false;
                    }
                case 'L':
                    if (cardCheckColor(value))
                    {
                        printLine("SERWER: JEST KOLOR WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA KOLORU WSROD ZAWODNIKOW!");
                        return false;
                    }
                case 'S':
                    if (cardCheckStraight(value))
                    {
                        printLine("SERWER: JEST STRIT WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA STRITA WSROD ZAWODNIKOW!");
                        return false;
                    }

                case 'T':
                    if (cardCheckThreeOfaKind(value))
                    {
                        printLine("SERWER: JEST TROJKA WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA TROJKI WSROD ZAWODNIKOW!");
                        return false;
                    }


                case 'R':
                    if (cardCheckPair(value))
                    {
                        printLine("SERWER: JEST PARA WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA PARY WSROD ZAWODNIKOW!");
                        return false;
                    }

                case 'V':
                    if (cardCheckHighCard(value))
                    {
                        printLine("SERWER: JEST NAJWYZASZA WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA NAJWYZSZEJ WSROD ZAWODNIKOW!");
                        return false;
                    }
            }
            return false;


        }
        public bool playerSayCheckTwoValues(char choose, string value1, string value2)
        {
            if (value1 == "10")
            {
                value1 = "T";
            }
            if (value2 == "10")
            {
                value2 = "T";
            }

            switch (choose)
            {

                case 'F':
                    if (cardCheckFullHouse(value1, value2))
                    {
                        printLine("SERWER: JEST FULL WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA FULLA WSROD ZAWODNIKOW!");
                        return false;
                    }


                case 'W':
                    if (cardCheckTwoPair(value1, value2))
                    {
                        printLine("SERWER: SA DWIE PARY WSROD ZAWODNIKOW!");
                        return true;
                    }
                    else
                    {
                        printLine("SERWER: NIE MA PAR WSROD ZAWODNIKOW!");
                        return false;
                    }
            }
            return false;

        }
        public void endOfGame()
        {
           string endText="KONIEC ";
            losePlayers.Reverse();
            string[] playersLScore = losePlayers.ToArray();
            
            foreach(string p in playersLScore)
            {
                endText = endText + p+" ";

            }
            foreach(Player s in startGamers)
            {
              sendMsgToPlayer(s.getIP(), endText);
            }
            timer1.Enabled = false;
            printLine("[" + DateTime.Now + "]"+"KONIEC GRY");
            file.Close();
            System.Threading.Thread.Sleep(2000000);
            
        }
        public bool stackIncludeCardINT(char color, int value_int)
        {

            char value = (char)value_int;
            Array cards;
            cards = stack.ToArray();
            string card_value_color = value.ToString() + color.ToString();
            foreach (Card card in cards)
            {
                if (card.getCardColorAndValue() == card_value_color)
                {
                    return true;
                }

            }
            return false;
        }
        public bool stackIncludeCardCHAR(char color, char value)
        {
            Array cards;
            cards = stack.ToArray();
            string val;

            if (value == 'T')
            {
                val = "10";
            }
            else
            {
                val = value.ToString();
            }

            string card_value_color = val + color.ToString();
            foreach (Card card in cards)
            {
                if (card.getCardColorAndValue() == card_value_color)
                {
                    return true;
                }

            }
            return false;
        }
        private bool checkStack(Card checkingCard)
        {
            Array cards;
            cards = stack.ToArray();
            foreach (Card card in cards)
            {
                if (card.getCardColorAndValue() == checkingCard.getCardColorAndValue())
                {
                    return true;
                }
            }
            return false;

        }

        public bool cardCheckPoker(string value, char color)
        {
            string[] values = new string[] { "2", "3", "4", "5", "6", "7", "8", "9", "T", "W", "D", "K", "A" };
            int startValue = 0;

            for (int x = 0; x <= 12; x++)
            {
                if (values[x] == value)
                {
                    startValue = x;
                    break;
                }
            }

            int goodPoint = 0;
            for (int i = 0; i <= 4; i++)
            {
                if (this.stackIncludeCardCHAR(color, values[i + startValue][0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 5)
            {
                return true;
            }
            return false;
        }
        public bool cardCheckFourOfaKind(string value)
        {
            string litery = "SHDC";
            int goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {
                if (this.stackIncludeCardCHAR(litery[lit], value[0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 4)
            {
                return true;
            }


            return false;
        }
        public bool cardCheckFullHouse(string Tvalue, string Dvalue)
        {

            string litery = "SHDC";
            //Szukaj trojki
            char trojka_value = '0';
            int goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {
                if (this.stackIncludeCardCHAR(litery[lit], Tvalue[0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 3)
            {
                trojka_value = Tvalue[0];
            }
            if (trojka_value == '0')
            {
                return false;
            }
            //Szukanie dwojki
            goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {
                if (this.stackIncludeCardCHAR(litery[lit], Dvalue[0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 2)
            {
                return true;
            }
            return false;
        }
        public bool cardCheckColor(string litera)
        {
            int goodPoint = 0;
            string[] values = new string[] { "2", "3", "4", "5", "6", "7", "8", "9", "T", "W", "D", "K", "A" };
            for (int i = 0; i <= 12; i++)
            {
                if (this.stackIncludeCardCHAR(litera[0], values[i][0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 5)
            {
                return true;
            }

            return false;
        }
        public bool cardCheckStraight(string value)
        {
            string litery = "SHDC";
            string[] values = new string[] { "2", "3", "4", "5", "6", "7", "8", "9", "T", "W", "D", "K", "A" };
            int startValue = 0;

            for (int x = 0; x <= 12; x++)
            {
                if (values[x] == value)
                {
                    startValue = x;
                    break;
                }
            }
            int goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {

                for (int i = 0; i <= 4; i++)
                {
                    if (this.stackIncludeCardCHAR(litery[lit], values[i + startValue][0]))
                    {
                        goodPoint++;
                    }
                }
                if (goodPoint >= 5)
                {
                    return true;
                }
            }
            return false;
        }
        public bool cardCheckThreeOfaKind(string value)
        {
            string litery = "SHDC";
            int goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {
                if (this.stackIncludeCardCHAR(litery[lit], value[0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 3)
            {
                return true;
            }
            return false;
        }
        public bool cardCheckTwoPair(string value1, string value2)
        {
            string litery = "SHDC";
            for (int x = 0; x < 2; x++)
            {
                int goodPoint = 0;
                for (int lit = 0; lit < 4; lit++)
                {
                    if (this.stackIncludeCardCHAR(litery[lit], value1[0]))
                    {
                        goodPoint++;
                    }
                }
                if (goodPoint >= 2)
                {
                    goodPoint = 0;
                    for (int lit = 0; lit < 4; lit++)
                    {
                        if (this.stackIncludeCardCHAR(litery[lit], value2[0]))
                        {
                            goodPoint++;
                        }
                    }
                    if (goodPoint >= 2)
                    {
                        return true;
                    }
                }
                string tmp;
                tmp = value1;
                value1 = value2;
                value2 = tmp;
            }
            return false;
        }
        public bool cardCheckPair(string value)
        {
            string litery = "SHDC";
            int goodPoint = 0;
            for (int lit = 0; lit < 4; lit++)
            {
                if (this.stackIncludeCardCHAR(litery[lit], value[0]))
                {
                    goodPoint++;
                }
            }
            if (goodPoint >= 2)
            {
                return true;
            }
            return false;
        }
        public bool cardCheckHighCard(string value)
        {
            string litery = "SHDC";
            char max = '0';
            int maxIter = 0;
            string[] values = new string[] { "2", "3", "4", "5", "6", "7", "8", "9", "T", "W", "D", "K", "A" };
            for (int lit = 0; lit < 4; lit++)
            {
                for (int i = 0; i <= 12; i++)
                {
                    if (this.stackIncludeCardCHAR(litery[lit], values[i][0]) && i > maxIter)
                    {
                        maxIter = i;
                    }
                }
            }
            if (value[0] == values[maxIter][0])
            {
                return true;
            }

            return false;
        }

        private void showStack()
        {
            Array cards;
            cards = stack.ToArray();
            printLine("---------------------------------------------- \n");
            printLine("ROZDANE KARTY: \n");
            foreach (Card card in cards)
            {
                printLine("|  " + card.getCardColorAndValue() + " " + card.getPlayerIp() + "  |");
            }
            printLine("---------------------------------------------- \n");
        }
        private string getPlayerCardsFromStack(string ip)
        {
            string value="";
            Array cards;
            cards = stack.ToArray();
            foreach (Card card in cards)
            {
                if(card.getPlayerIp()== ip)
                {
                    value = value + card.getCardColorAndValue() + " ";
                }
            }
            return value;

        }
        private void addToStack(Card card)
        {
            stack.Push(card);
        }
        private void clearStack()
        {
            stack.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try {

                    timerTime++;
                    if (PlayerL.Count == playerCount && timerTime % playerTime == 0 && this.getGameStarted() == true)
                    {
                        yourRound = true;
                    
                        if (startPlayerID == 0)
                        {
                            if (PlayerL[startPlayerID].getOnTheGame() == true)
                            {
                                sendMsgToPlayer(PlayerL[startPlayerID].getIP(), "TWOJ RUCH");
                                PlayerL[startPlayerID].setYourRound(true);
                            }
                        }
                        else
                        {
                            sendMsgToPlayer(PlayerL[startPlayerID - 1].getIP(), "TWOJ RUCH");
                            PlayerL[startPlayerID - 1].setYourRound(true);

                        }
                    

                    }
                    else if (this.getGameStarted() == true && PlayerL.Count == playerCount && timerTime % playerTime == (playerTime * 99 / 100) && yourRound == true)
                    {

                        if (startPlayerID == 0)
                        {
                            if (PlayerL[startPlayerID].getOnTheGame() == true)
                            {
                                PlayerL[startPlayerID].setYourRound(false);
                            }
                         }
                        else
                        {
                            PlayerL[startPlayerID - 1].setYourRound(false);
                        }
                        startPlayerID++;
                        yourRound = false;
                    }
                    if ((startPlayerID - 1) == playerCount)
                    {
                        startPlayerID = 1;
                    }
                
            }
            catch(System.ArgumentOutOfRangeException)
            {
                Console.WriteLine("TICKER");
            }
        }
        public bool getGameStarted()
        {
            return gameStarted;
        }
        public void setGameStarted(bool tr)
        {
            gameStarted = tr;
        }
        public int getReadyPlayers()
        {
            return readyPlayers;
        }
        public void setNextReadyPlayer()
        {
            readyPlayers++;
        }
        public void setRemoveReadyPlayer()
        {
            readyPlayers--;
        }
        public void setReadyPlayers(int count)
        {
            readyPlayers = count;
        }
        private void outPlayerFromGame()
        {
            Player kicker = null;
            foreach (Player p in PlayerL)
            {
                if(((p.getPoints()==5 && p.getOnTheGame()==true)) || playerCount == 1)
                {
                    kicker = p;
                    break;
                }
            }
            PlayerL.Remove(kicker);
            playerCount--;
            kicker.setOnTheGame(false);
            lbClients.Items.Remove(kicker.getIP().ToString());
            
        }
        public bool ipUsed(string ipek)
        {
            foreach(Player p in PlayerL)
            {
                if(p.getIP()==ipek)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
